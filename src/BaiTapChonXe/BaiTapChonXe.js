import React, { Component } from "react";
import { connect } from "react-redux";

class BaiTapChonXe extends Component {
  render() {
    return (
      <div className="container py-5">
        <div className="row">
          <div className="col-6">
            <img className="w-100" src={this.props.giaTri} alt="" />
          </div>
          <div className="py-5">
            <button
              onClick={this.props.handleChangeRedCar}
              className="btn btn-danger ml-5"
            >
              Red
            </button>
            <button
              onClick={this.props.handleChangeBlackCar}
              className="btn btn-dark mx-4"
            >
              Black
            </button>
            <button
              onClick={this.props.handleChangeSilverCar}
              className="btn btn-secondary"
            >
              Silver
            </button>
          </div>
        </div>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    giaTri: state.carReducer.carImg,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handleChangeRedCar: () => {
      dispatch({
        type: "Change red car",
      });
    },
    handleChangeBlackCar: () => {
      dispatch({
        type: "Change black car",
      });
    },
    handleChangeSilverCar: () => {
      dispatch({
        type: "Change silver car",
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(BaiTapChonXe);
