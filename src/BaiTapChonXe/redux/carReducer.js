let initialState = {
  carImg: "./img/car/black-car.jpg",
};

export const carReducer = (state = initialState, action) => {
  switch (action.type) {
    case "Change red car": {
      state.carImg = "./img/car/red-car.jpg";
      return { ...state };
    }
    case "Change black car": {
      state.carImg = "./img/car/black-car.jpg";
      return { ...state };
    }
    case "Change silver car": {
      state.carImg = "./img/car/silver-car.jpg";
      return { ...state };
    }
    default:
      return state;
  }
};
