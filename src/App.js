import logo from "./logo.svg";
import "./App.css";
import BaiTapChonXe from "./BaiTapChonXe/BaiTapChonXe";

function App() {
  return (
    <div className="App">
      <BaiTapChonXe />
    </div>
  );
}

export default App;
